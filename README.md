# README #

This is just a POC on how to use flyway with the spring boot and flyway. It will use mysql server as the main server and H2 as in-memory server, which will used to run the test cases for the endpoints.
This is using hibernate as the JPA framework along with the spring boot 2.0

### What is this repository for? ###

* This repostiory contains the sample code for spring boot and flyway integration.
* Version 1.0

### How do I get set up? ###

* just clone this repository , run the maven clean install and you are good to go.

