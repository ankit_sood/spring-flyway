package org.regal.app.controller;

import java.util.List;
import java.util.Optional;

import org.regal.app.model.Account;
import org.regal.app.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController("/account")
public class AccountController {
	
	private AccountService accountService;
	
	public AccountController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@GetMapping("/{accountNumber}")
	public void getAccountDetail(@PathVariable("accountNumber") String accountNumber) {
		
	}
	
	@GetMapping
	public Optional<List<Account>> getAccountDetails() {
		return accountService.getAccounts();
	}

}
