package org.regal.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegalBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegalBankApplication.class, args);
	}
}
