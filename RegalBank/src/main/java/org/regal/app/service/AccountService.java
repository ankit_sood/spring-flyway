package org.regal.app.service;

import java.util.List;
import java.util.Optional;

import org.regal.app.model.Account;
import org.regal.app.repository.AccountRepository;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
	private AccountRepository accountRepository;
	
	public AccountService(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	public Optional<List<Account>> getAccounts() {
		return Optional.ofNullable(accountRepository.findAll());
	}
	
}
